package com.telstra.codechallenge.controller;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.telstra.codechallenge.model.Quote;
import com.telstra.codechallenge.service.QuotesService;

@RestController
public class QuotesController {
	
	private final Logger log=LoggerFactory.getLogger(QuotesController.class);
	
	  @Autowired
	  private QuotesService quotesService;
	 
	  @GetMapping("/quotes")
	  public List<Quote> quotes() {
		 log.info(quotesService.getQuotes().toString());
	    return quotesService.getQuotes();
	  }

	  @GetMapping("/quotes/random")
	  public Quote quote() {
		  log.info(quotesService.getRandomQuote().toString());
	    return quotesService.getRandomQuote();
	  }
	  
	  @GetMapping("/quotes/random/app")
	  public String getHystrix() {
		  return quotesService.getHysrix();
	  }
	  
	  @GetMapping("/test/{name}")
	  public String greeting(@PathVariable String name){
		 log.debug("Request {}",name);
		return "welcome to infosys";
	  }
	  
	  
}
